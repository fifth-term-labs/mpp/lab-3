﻿using System.Collections.Generic;
using System.ServiceModel;

namespace FeedServiceHost
{
    [ServiceContract]
    public interface IFeedService
    {
        [OperationContract]
        List<FeedItem> GetFeed(string feedUrl, List<string> categories);

        [OperationContract]
        void SendFeedByEmail(string feedUrl, List<string> categories, List<string> recipients);
    }
}