﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace FeedServiceHost
{
    internal static class Program
    {
        private static void Main()
        {
            using (var host = new ServiceHost(typeof(FeedService)))
            {
                host.Open();
                Console.WriteLine("Feed service started (http://localhost:8080/FeedService)");
                Console.ReadLine();
            }
        }
    }
}
