﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceModel.Syndication;
using System.Xml;

namespace FeedServiceHost
{
    public class FeedService : IFeedService
    {
        private const string AppEmailUsername = "svbilalow@mail.ru";
        private const string AppEmailPassword = "sjjdxxnHvz23fmwayf";

        public List<FeedItem> GetFeed(string feedUrl, List<string> categories)
        {
            return LoadFeedItems(feedUrl, categories);
        }

        private static List<FeedItem> LoadFeedItems(string url, IReadOnlyList<string> categories)
        {
            var rssItems = new List<SyndicationItem>();
            var reader = XmlReader.Create(url);
            var feed = SyndicationFeed.Load(reader);

            if (categories.Count == 0 || string.IsNullOrEmpty(categories[0]))
            {
                rssItems.AddRange(feed.Items);
            }
            else
            {
                rssItems.AddRange(from feedItem in feed.Items
                    from category in categories
                    where feedItem.Categories[0].Name == category
                    select feedItem);
            }

            reader.Close();

            return rssItems
                .Select(rssItem =>
                    new FeedItem(
                        rssItem.Title.Text,
                        rssItem.Categories[0].Name,
                        rssItem.Links[0].Uri.AbsoluteUri,
                        rssItem.PublishDate.ToString()
                    )
                )
                .ToList();
        }

        public void SendFeedByEmail(string feedUrl, List<string> categories, List<string> recipients)
        {
            var client = new SmtpClient
            {
                Port = 25,
                Host = "smtp.mail.ru",
                EnableSsl = true,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(AppEmailUsername, AppEmailPassword)
            };

            var feedItems = GetFeed(feedUrl, categories);
            var messageBody = feedItems.Aggregate(
                "",
                (current, item) => current + item.Url + "\n"
            );

            var mailMessages = recipients
                .Select(recipient =>
                    new MailMessage(AppEmailUsername, recipient, "RSS", messageBody)
                );

            foreach (var mailMessage in mailMessages)
            {
                client.Send(mailMessage);
            }
        }
    }
}