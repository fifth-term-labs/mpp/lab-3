﻿using System.Runtime.Serialization;

namespace FeedServiceHost
{
    [DataContract]
    public class FeedItem
    {
        [DataMember] public string Title { get; set; }

        [DataMember] public string Category { get; set; }

        [DataMember] public string Url { get; set; }

        [DataMember] public string PublishDate { get; set; }

        public FeedItem(string title, string category, string url, string publishDate)
        {
            Title = title;
            Category = category;
            Url = url;
            PublishDate = publishDate;
        }
    }
}