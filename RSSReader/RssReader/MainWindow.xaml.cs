﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace RssReader
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private readonly List<string> _rssUrls = new List<string>
        {
            "https://nn.by/?c=rss-top&lang=ru"
        };

        private readonly List<SyndicationItem> _feedItems = new List<SyndicationItem>();

        private void BUpdateItem_Click(object sender, RoutedEventArgs e)
        {
            LoadFeeds();
            PrintFeeds();
        }

        private void LoadFeeds()
        {
            _feedItems.Clear();
            foreach (var url in _rssUrls)
            {
                var reader = XmlReader.Create(url);
                var feed = SyndicationFeed.Load(reader);

                foreach (var feedItem in feed.Items)
                {
                    _feedItems.Add(feedItem);
                }

                reader.Close();
            }
        }

        private void PrintFeeds()
        {
            LBItems.Items.Clear();
            foreach (var listBoxItem in _feedItems.Select(feedItem => new ListBoxItem {Content = feedItem.Title.Text}))
            {
                LBItems.Items.Add(listBoxItem);
            }
        }

        private void LBItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LBItems.SelectedIndex <= -1) return;

            var item = _feedItems[LBItems.SelectedIndex];
            LDate.Content = item.PublishDate.ToString();
            LTitle.Content = item.Title.Text;
            LURL.Content = item.Links[0].Uri.AbsoluteUri;
            LCategory.Content = item.Categories[0].Name;
        }

        private void BRead_Click(object sender, RoutedEventArgs e)
        {
            var index = LBItems.SelectedIndex;

            if (index > -1)
            {
                Process.Start(_feedItems[index].Links[0].Uri.AbsoluteUri);
            }
        }
    }
}