﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceModel.Syndication;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace RssReaderMultiThread
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private readonly List<string> _rssUrls = new List<string>
        {
            "https://nn.by/?c=rss-top&lang=ru"
        };

        private readonly List<SyndicationItem> _feedItems = new List<SyndicationItem>();

        private const char Separator = '|';

        private void BUpdateItem_Click(object sender, RoutedEventArgs e)
        {
            LoadFeeds();
            PrintFeeds();
        }

        private void LoadFeeds()
        {
            var categories = TBCategory.Text.Split(Separator);

            _feedItems.Clear();
            var items = new List<SyndicationItem>();
            var waitHandle = new ManualResetEvent(false);
            foreach (var url in _rssUrls)
            {
                ThreadPool.QueueUserWorkItem(state =>
                {
                    items.AddRange(LoadFeed(url, categories));
                    waitHandle.Set();
                });

                waitHandle.WaitOne();
            }

            _feedItems.AddRange(items);
        }

        private static IEnumerable<SyndicationItem> LoadFeed(string url, IReadOnlyList<string> categories)
        {
            var items = new List<SyndicationItem>();
            var reader = XmlReader.Create(url);
            var feed = SyndicationFeed.Load(reader);

            if (string.IsNullOrEmpty(categories[0]))
            {
                items.AddRange(feed.Items);
                reader.Close();
                return items;
            }

            items.AddRange(from feedItem in feed.Items
                from category in categories
                where feedItem.Categories[0].Name == category
                select feedItem);

            reader.Close();
            return items;
        }

        private void PrintFeeds()
        {
            LBItems.Items.Clear();
            foreach (var listBoxItem in _feedItems.Select(feedItem => new ListBoxItem {Content = feedItem.Title.Text}))
            {
                LBItems.Items.Add(listBoxItem);
            }
        }

        private void LBItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LBItems.SelectedIndex <= -1) return;

            var item = _feedItems[LBItems.SelectedIndex];
            LDate.Content = item.PublishDate.ToString();
            LTitle.Content = item.Title.Text;
            LURL.Content = item.Links[0].Uri.AbsoluteUri;
            LCategory.Content = item.Categories[0].Name;
        }

        private void BRead_Click(object sender, RoutedEventArgs e)
        {
            var index = LBItems.SelectedIndex;
            if (index > -1)
                Process.Start(_feedItems[index].Links[0].Uri.AbsoluteUri);
        }

        private void BSend_Click(object sender, RoutedEventArgs e)
        {
            var messageRecipients = TBEmail.Text.Split(Separator);

            if (messageRecipients.Length == 0 || string.IsNullOrEmpty(messageRecipients[0]))
            {
                MessageBox.Show("Message recipient field can't be empty.");
                return;
            }

            var messageBody = _feedItems.Aggregate(
                "",
                (current, item) => current + item.Links[0].Uri.AbsoluteUri + "\n"
            );

            ThreadPool.QueueUserWorkItem(state => SendEmail(messageRecipients, messageBody));
        }

        private static void SendEmail(IEnumerable<string> messageRecipients, string messageBody)
        {
            const string appEmailUsername = "svbilalow@mail.ru";
            const string appEmailPassword = "sjjdxxnHvz23fmwayf";

            var client = new SmtpClient
            {
                Port = 25,
                Host = "smtp.mail.ru",
                EnableSsl = true,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(appEmailUsername, appEmailPassword)
            };

            foreach (var messageRecipient in messageRecipients)
            {
                var mail = new MailMessage(appEmailUsername, messageRecipient, "RSS", messageBody);
                client.Send(mail);
            }
        }
    }
}